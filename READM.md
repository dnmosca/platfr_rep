**Test API Platfr**

Realizzare un applicativo che permetta di gestire le seguenti operazioni sul conto:

* Lettura saldo
* Bonifico

L’applicativo dovrà essere sviluppato utilizzando le API esposte da Platfr.io, la documentazione è disponibile online https://www.platfr.io/#/docs.

Per la fase di sviluppo e test sarà sufficiente l’utilizzo della versione MOCK della API, non sono necessarie credenziali particolari per l’accesso. Si tratta quindi di utilizzare l’ambiente free beta.

Proprietà/Costanti applicativo
* {accountId} : Long, è il numero conto di riferimento, nelle API è sempre indicato come {accountId}

*Operazione: Lettura saldo*
	API: https://www.platfr.io/#/docs/api#Account%20Balance
*	Input: {accountId}:Long è un parametro dell’applicazione
* Output: Visualizzare il saldo

*Operazione: Bonifico*
	API: https://www.platfr.io/#/docs/api#v2.1-POST-BankingAccountCreateSCTOrder
Input:

*	{accountId}:Long è un parametro dell’applicazione
*	{receiverName}:String, è il beneficiario del bonifico
* {description}: String, descrizione del bonifico
*	{currency}:String
*	{amount}:String 
*	{executionDate}:String DD/MM/YYYY
*	
Output: Stato dell’operazione;

Non è necessario lo sviluppo di un interfaccia utente;
Tecnologie
Le seguenti tecnologie sono consigliate per lo sviluppo, ma il candidato potrà scegliere o proporre soluzioni anche in altri linguaggi.

*	Java / Spring
*	Java
*	JavaScript

**Artefatto**
Il codice sorgente dell’artefatto prodotto dovrà essere pubblicato e condiviso tramite:

*	GitHub
*	Bitbucket
*	Altri strumenti similari di version control

**Punti chiave**

L’obiettivo è valutare la capacità di sviluppo software con particolare attenzione a:

*	Struttura del codice
*	Approccio al problema
*	Test automatizzati

