package client;

import java.awt.List;
import java.io.*;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.NameValuePair;

public class PostSCTOrderClient {


	private HttpClient client = HttpClientBuilder.create().build();

	public static void main(String[] args) { 

	    String url = "https://sandbox.platfr.io/api/gbs/banking/v2.1/accounts/1/payments/sct/orders";
		
		PostSCTOrderClient http = new PostSCTOrderClient();

		try {
			
			http.postSCTOrder(url);
					
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	private void postSCTOrder(String url) throws Exception {

		HttpPost post = new HttpPost(url);

		post.setHeader("Content-type","application/json");
		post.setHeader("Auth-Schema","S2S");

		ArrayList<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("accountId", "1"));
		urlParameters.add(new BasicNameValuePair("receiverName", "John Doe"));
		urlParameters.add(new BasicNameValuePair("description", "test"));
		urlParameters.add(new BasicNameValuePair("currency", "EUR"));
		urlParameters.add(new BasicNameValuePair("amount", "800.00"));
		urlParameters.add(new BasicNameValuePair("executionDate", "15/01/2019"));

		post.setEntity(new UrlEncodedFormEntity(urlParameters));

		HttpResponse response = client.execute(post);

		System.out.println("Response Code : " 
	                + response.getStatusLine().getStatusCode());

		BufferedReader rd = new BufferedReader(
		        new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}

		System.out.println(result.toString());
	}

}