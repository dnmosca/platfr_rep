package client;

import java.io.*;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

public class AccountBalanceClient {


	private HttpClient client = HttpClientBuilder.create().build();

	public static void main(String[] args) { 

	    String url = "https://sandbox.platfr.io/api/gbs/banking/v2/accounts/1/balance";
		
		AccountBalanceClient http = new AccountBalanceClient();

		try {
			
			String res = http.getAccountBalance(url);
					
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	private String getAccountBalance(String url) throws Exception {

		HttpGet request = new HttpGet(url);

		request.setHeader("Content-type","application/json");
		request.setHeader("Auth-Schema","S2S");

		HttpResponse response = client.execute(request);
		
		System.out.println("Response Code : " 
                + response.getStatusLine().getStatusCode());

		BufferedReader rd = new BufferedReader(
				new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}

		return result.toString();

	}

}